function submit_lead() {

    // Local
    // var request_url = "http://eezycrm/api/addlead";

    // Taika Crm
    var request_url = "https://lms.hoteltaika.com/api/addlead";

    var source_id = 3;
    var status_id = 3;
    var assigned_to = 2;

    var get_name = $("#name").val();
    var get_mobile = $("#phonenumber").val();
    var get_check_in = $("#check_in").val();
    var get_check_out = $("#check_out").val();

    var get_email = $("#email").val() ? $("#email").val() : "";
    var no_of_persons = $("#no_of_person").val() ? $("#no_of_person").val() : "";
    var no_of_childs = $("#no_of_child").val() ? $("#no_of_child").val() : "";

    if (!get_name) {
        alert("Please Enter Valid Name");
    } else if (!get_mobile) {
        alert("Please Enter Valid Mobile Number");
    } else if (!get_check_in) {
        alert("Please Select Check In");
    } else if (!get_check_out) {
        alert("Please Select Check Out");
    } else {
        // Phone number validation
        if (get_mobile.length < 10) {
            alert("Phone number must be at least 10 digits.");
            return; 
        }

        $('#submit_lead_btn').prop('disabled', true);

        var form_data = {
            "status": status_id,
            "source": source_id,
            "assigned": assigned_to,
            "tags": "",
            "name": get_name,
            "email": get_email,
            "website": "",
            "phonenumber": get_mobile,
            "no_of_childs": no_of_childs,
            "no_of_persons": no_of_persons,
            "check_out": get_check_out,
            "address": "",
            "city": "",
            "state": "",
            "country": "102",
            "zip": "",
            "default_language": "",
            "check_in": get_check_in,
            "description": "",
            "custom_contact_date": "",
            "contacted_today": "on",
            "address": ""
        };

        console.log(form_data);

        // Submit the form via AJAX
        $.ajax({
            type: "POST",
            url: request_url,
            data: JSON.stringify(form_data),
            encode: true,
        }).done(function(data) {
            console.log(data);
            alert("Your Request Sent Successfully.");
            window.location.reload();
            $('#submit_lead_btn').prop('disabled', false);
        });
    }
}



function submit_lead_from_bookings() {

    // Local
    // var request_url = "http://eezycrm/api/addlead";

    //Taika Crm
    var request_url = "https://lms.hoteltaika.com/api/addlead";


    var source_id = 3;
    var status_id = 3;
    var assigned_to = 2;



    var get_name = $("#name_1").val();
    var get_mobile = $("#phonenumber_1").val();
    var get_check_in = $("#check_in_1").val();
    var get_check_out = $("#check_out_1").val();


    var get_email = $("#email_1").val() ? $("#email_1").val() : "";
    var no_of_persons = $("#no_of_person_1").val() ? $("#no_of_person_1").val() : "";
    var no_of_childs = $("#no_of_child_1").val() ? $("#no_of_child_1").val() : "";

    if (!get_name) {
        alert("Please Enter Valid Name");
    } else if (!get_mobile) {
        alert("Please Enter Valid Mobile Number");
    } else if (!get_check_in) {
        alert("Please Select Check In");
    } else if (!get_check_out) {
        alert("Please Select Check Out");
    } else {
          // Phone number validation
          if (get_mobile.length < 10) {
            alert("Phone number must be at least 10 digits.");
            return; 
        }
        $('#submit_lead_btn1').prop('disabled', true);
        var form_data = {
            "status": status_id,
            "source": source_id,
            "assigned": assigned_to,
            "tags": "",
            "name": get_name,
            "email": get_email,
            "website": "",
            "phonenumber": get_mobile,
            "no_of_childs": no_of_childs,
            "no_of_persons": no_of_persons,
            "check_out": get_check_out,
            "address": "",
            "city": "",
            "state": "",
            "country": "102",
            "zip": "",
            "default_language": "",
            "check_in": get_check_in,
            "description": "",
            "custom_contact_date": "",
            "contacted_today": "on",
            "address": ""
        };
        console.log(form_data)

        $.ajax({
            type: "POST",
            url: request_url,
            data: JSON.stringify(form_data),
            encode:true,
        }).done(function(data) {
            console.log(data)
        $('#submit_lead_btn1').prop('disabled', false);
            alert("Your Request Sent Successfully.")
            window.location.reload()
        });
    }


}