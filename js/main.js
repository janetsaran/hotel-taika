(function ($) {

    // $("#check_in").focus( function() {
    //     $(this).attr({type: 'datetime-local'});
    // });
    // $("#check_out").focus( function() {
    //     $(this).attr({type: 'datetime-local'});
    // });

    $(document).ready(function () {
        const currentYear = new Date().getFullYear(); // Get the current year
    
       
        let checkInDate1 = null;
    
        $('#check_in_1').datetimepicker({
            format: 'd-m-Y H:i',
            yearStart: currentYear, 
            yearEnd: currentYear + 2, 
            minDate: new Date(currentYear, 0, 1), 
            maxDate: new Date(currentYear + 2, 11, 31), 
            onChangeDateTime: function (selectedDate) {
                if (selectedDate) {
                    checkInDate1 = selectedDate;
                    $('#check_out_1').datetimepicker('setOptions', {
                        minDate: selectedDate, 
                        maxDate: new Date(currentYear + 2, 11, 31) 
                    });
                }
            }
        });
    
        $('#check_out_1').datetimepicker({
            format: 'd-m-Y H:i',
            yearStart: currentYear, 
            yearEnd: currentYear + 2,
            minDate: new Date(currentYear, 0, 1), 
            maxDate: new Date(currentYear + 2, 11, 31), 
            beforeShowDay: function (date) {
                if (checkInDate1 && date < checkInDate1) {
                    return [false, "", "Disabled"];
                }
                return [true];
            }
        });
    
       
        let checkInDate2 = null;
    
        $('#check_in').datetimepicker({
            format: 'd-m-Y H:i',
            yearStart: currentYear, 
            yearEnd: currentYear + 2, 
            minDate: new Date(currentYear, 0, 1), 
            maxDate: new Date(currentYear + 2, 11, 31), 
            onChangeDateTime: function (selectedDate) {
                if (selectedDate) {
                    checkInDate2 = selectedDate;
                    $('#check_out').datetimepicker('setOptions', {
                        minDate: selectedDate, 
                        maxDate: new Date(currentYear + 2, 11, 31) 
                    });
                }
            }
        });
    
        $('#check_out').datetimepicker({
            format: 'd-m-Y H:i',
            yearStart: currentYear, 
            yearEnd: currentYear + 2, 
            minDate: new Date(currentYear, 0, 1),
            maxDate: new Date(currentYear + 2, 11, 31), 
            beforeShowDay: function (date) {
                if (checkInDate2 && date < checkInDate2) {
                    return [false, "", "Disabled"];
                }
                return [true];
            }
        });
    });
    
    "use strict";

  

    // Spinner
    var spinner = function () {
        setTimeout(function () {
            if ($('#spinner').length > 0) {
                $('#spinner').removeClass('show');
            }
        }, 1);
    };
    spinner();
    
    
    // Initiate the wowjs
    new WOW().init();
    
    
    // Dropdown on mouse hover
    const $dropdown = $(".dropdown");
    const $dropdownToggle = $(".dropdown-toggle");
    const $dropdownMenu = $(".dropdown-menu");
    const showClass = "show";
    
    $(window).on("load resize", function() {
        if (this.matchMedia("(min-width: 992px)").matches) {
            $dropdown.hover(
            function() {
                const $this = $(this);
                $this.addClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "true");
                $this.find($dropdownMenu).addClass(showClass);
            },
            function() {
                const $this = $(this);
                $this.removeClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "false");
                $this.find($dropdownMenu).removeClass(showClass);
            }
            );
        } else {
            $dropdown.off("mouseenter mouseleave");
        }
    });
    
    
    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 1500, 'easeInOutExpo');
        return false;
    });


    // Facts counter
    $('[data-toggle="counter-up"]').counterUp({
        delay: 10,
        time: 2000
    });


    // Modal Video
    $(document).ready(function () {
        var $videoSrc;
        $('.btn-play').click(function () {
            $videoSrc = $(this).data("src");
        });
        console.log($videoSrc);

        $('#videoModal').on('shown.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        })

        $('#videoModal').on('hide.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc);
        })
    });


    // Testimonials carousel
    $(".testimonial-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        margin: 25,
        dots: false,
        loop: true,
        nav : true,
        navText : [
            '<i class="bi bi-arrow-left"></i>',
            '<i class="bi bi-arrow-right"></i>'
        ],
        responsive: {
            0:{
                items:1
            },
            768:{
                items:2
            }
        }
    });
    


})(jQuery);

 


